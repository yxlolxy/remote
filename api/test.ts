import type { VercelRequest, VercelResponse } from '@vercel/node';
import { kv } from '@vercel/kv';

const getHandler = async (
    request: VercelRequest,
    response: VercelResponse,
) => {
    const key = request.query?.['key']
    if (key === undefined) {
        response.status(200).json({
            error: null,
            data: null,
        })
        return 
    }
    const data = await kv.get(key as string)
    response.status(200).json({
        error: null,
        data,
    })
}
const postHandler = async (
    request: VercelRequest,
    response: VercelResponse,
) => { 
    const body = request.body
    let count = 0
    if (typeof body === 'object' && body) {
        for(let key in body) {
            if (body.hasOwnProperty(key)) {
                await kv.set(key, body[key])
                count++
            }
        }
    }
    response.status(200).json({
        error: null,
        data: count,
    })
}

const handler = async (
    request: VercelRequest,
    response: VercelResponse,
) => {
    const method = request.method?.toLocaleLowerCase() ?? ''
    switch (method) {
        case 'get': {
            getHandler(request, response)
            break
        }
        case 'post': {
            postHandler(request, response)
            break
        }
        default: {
            response.status(400)
        }
    }
}

export default handler
